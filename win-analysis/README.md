# win-analysis

Parse Windows Portable Executable (PE) File and dump it.


## Usage

```bash
> win-analysis /path/to/executable.dll
```